import React from 'react';
import MovieCard from '../components/MovieCard';

import { movies } from './stub';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Card/Movie card',
  component: MovieCard
};

const Template = (args) => <MovieCard {...args} />;

export const Primary = Template.bind({})

Primary.args ={
  movie: movies[0]
}
