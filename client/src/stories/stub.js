export const movies = [
  {
    "_id": "6276dd44802ec260dafc8dbb",
    "image": "https://www.themoviedb.org/t/p/w220_and_h330_face/3ayWL13P1HeRnyVL9lU9flOdZjq.jpg",
    "releaseDate": "2014-04-13T12:38:32 -03:00",
    "title": "Poshome",
    "genres": [
      {
        id: 1,
        name: 'Drama'
      },
      {
        id: 2,
        name: 'Horror'
      }
    ],
    "runtime": 123
  },
  {
    "_id": "6276dd442e753f3bd3436598",
    "image": "https://www.themoviedb.org/t/p/w220_and_h330_face/8CXbCCGiJxi4AXPBQ1QPrehMIGG.jpg",
    "releaseDate": "2021-08-26T04:10:26 -03:00",
    "title": "Overfork"
  },
  {
    "_id": "6276dd44c551c911dc70ab43",
    "image": "https://www.themoviedb.org/t/p/w220_and_h330_face/jFZJEoPzt2RKSsZG8QEWptX5Xyw.jpg",
    "releaseDate": "2021-05-03T11:50:06 -03:00",
    "title": "Norsup"
  },
  {
    "_id": "6276dd44deb29bf65b955886",
    "image": "https://www.themoviedb.org/t/p/w220_and_h330_face/ixgnqO8xhFMb1zr8RRFsyeZ9CdD.jpg",
    "releaseDate": "2015-06-09T05:01:26 -03:00",
    "title": "Furnigeer"
  },
  {
    "_id": "6276dd449e60abbd06b9dc65",
    "image": "https://www.themoviedb.org/t/p/w220_and_h330_face/6jDc8zxYRr1jUEShDDPwLcH1Id9.jpg",
    "releaseDate": "2016-11-28T10:29:21 -02:00",
    "title": "Atgen"
  }
]